Kube-Events
https://github.com/max-rocket-internet/k8s-event-logger

Ingress-Controller
https://github.com/kubernetes/ingress-nginx/tree/main/charts/ingress-nginx

Metrics
https://github.com/kubernetes-sigs/metrics-server/tree/master/charts

Prometheus-Stack:
https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack